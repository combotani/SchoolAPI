var mongoose = require('mongoose');
var Teacher = require('../models/teachers');

exports.get_all = function(req, res, next) {
    var url = req.protocol + "://" + req.headers.host;
    Teacher
        .find()
        .select('_id name sex subject')
        .exec()
        .then(function(docs) {
            var response = {
                count: docs.length,
                teachers: docs.map(function(doc) {
                    return {
                        name: doc.name,
                        sex: doc.sex,
                        subject: doc.subject,
                        _id: doc._id,
                        request: {
                            type: "GET",
                            url: url + "/teachers/" + doc._id
                        }
                    }
                })
            }
            res.status(200).json(response);
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).json({ error: err });
        });
}

exports.get_teacher = function(req, res, next) {
    var url = req.protocol + "://" + req.headers.host;
    var id = req.params.teacherId;
    Teacher
        .findById(id)
        .select('_id name sex subject')
        .exec()
        .then(function(doc) {
            console.log(doc);
            if(doc){
                res.status(200).json({
                    Teacher: {
                        name: doc.name,
                        sex: doc.sex,
                        subject: doc.subject,
                        _id: doc._id,
                        request: {
                            type: "GET",
                            url: url + "/teachers"
                        }
                    }
                });
            }else{
                res.status(404).json({ message: "Invalid teacher ID" })
            }
        })
        .catch(function(err) {
            console.log(err)
            res.status(500).json({ error: err });
        });
}

exports.create_teacher = function(req, res, next) {
    var url = req.protocol + "://" + req.headers.host;
    var teacher = new Teacher({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        sex: req.body.sex,
        subject: req.body.subject
    });
    teacher
        .save()
        .then(function(result) {
            console.log(result);
            res.status(201).json({
                message: "Teacher created",
                createTeacher: {
                    name: result.name,
                    sex: result.sex,
                    subject: result.subject,
                    _id: result._id,
                    request: {
                        type: "GET",
                        url: url + "/teachers/" + result._id
                    }
                }
            });
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).json(err);
        });
}

exports.update_teacher = function(req, res, next) {
    var url = req.protocol + "://" + req.headers.host + req.originalUrl;
    var id = req.params.teacherId;
    var updateOps = {};
    for(var ops of req.body) {
        updateOps[ops.opsName] = ops.value;
    }
    Teacher.update({ _id: id }, { $set: updateOps })
        .exec()
        .then(function(result) {
            res.status(200).json({
                message: "Teacher updated",
                request: {
                    type: "GET",
                    url: url
                }
            });
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).json({ error: err });
        });
}

exports.delete_teacher = function(req, res, next) {
    var url = req.protocol + "://" + req.headers.host;
    var id = req.params.teacherId;
    Teacher.remove({ _id: id })
        .exec()
        .then(function(result) {
            res.status(200).json({
                message: "Teacher deleted",
                request: {
                    type: "POST",
                    url: url + "/teachers",
                    body: { name: "String", sex: "String", subject: "String" }
                }
            })
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).json({ error: err });
        });
}