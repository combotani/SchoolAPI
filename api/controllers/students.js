var mongoose = require('mongoose');
var Student = require('../models/students');

exports.get_all = function(req, res, next) {    
    var url = req.protocol + "://" + req.headers.host;
    Student
        .find()
        .select('_id name sex')
        .exec()
        .then(function(docs) {
            var response = {
                count: docs.length,
                students: docs.map(function(doc) {
                    return {
                        name: doc.name,
                        sex: doc.sex,
                        _id: doc._id,
                        request: {
                            type: "GET",
                            url: url + "/students/" + doc._id
                        }
                    }
                })
            }
            res.status(200).json(response);
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).json({ error: err });
        });
}

exports.get_student = function(req, res, next) {
    var url = req.protocol + "://" + req.headers.host;
    var id = req.params.studentId;
    Student
        .findById(id)
        .select('_id name sex')
        .exec()
        .then(function(doc) {
            console.log(doc);
            if(doc){
                res.status(200).json({
                    Student: {
                        name: doc.name,
                        sex: doc.sex,
                        _id: doc._id,
                        request: {
                            type: "GET",
                            url: url + "/students"
                        }
                    }
                });
            }else{
                res.status(404).json({ message: "Invalid student ID" })
            }
        })
        .catch(function(err) {
            console.log(err)
            res.status(500).json({ error: err });
        });
}

exports.create_student = function(req, res, next) {
    var url = req.protocol + "://" + req.headers.host;
    var student = new Student({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        sex: req.body.sex
    });
    student
        .save()
        .then(function(result) {
            console.log(result);
            res.status(201).json({
                message: "Student created",
                createStudent: {
                    name: result.name,
                    sex: result.sex,
                    _id: result._id,
                    request: {
                        type: "GET",
                        url: url + "/students/" + result._id
                    }
                }
            });
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).json(err);
        });
}

exports.update_student = function(req, res, next) {
    var url = req.protocol + "://" + req.headers.host + req.originalUrl;
    var id = req.params.studentId;
    var updateOps = {};
    for(var ops of req.body) {
        updateOps[ops.opsName] = ops.value;
    }
    Student.update({ _id: id }, { $set: updateOps })
        .exec()
        .then(function(result) {
            res.status(200).json({
                message: "Student updated",
                request: {
                    type: "GET",
                    url: url
                }
            });
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).json({ error: err });
        });
}

exports.delete_student = function(req, res, next) {
    var url = req.protocol + "://" + req.headers.host;
    var id = req.params.studentId;
    Student.remove({ _id: id })
        .exec()
        .then(function(result) {
            res.status(200).json({
                message: "Student deleted",
                request: {
                    type: "POST",
                    url: url + "/students",
                    body: { name: "String", sex: "String" }
                }
            })
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).json({ error: err });
        });
}