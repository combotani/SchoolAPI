var mongoose = require('mongoose');

var ClassObj = require('../models/classes');
var Teacher = require('../models/teachers');
var Student = require('../models/students');

exports.get_all = function(req, res, next) {    
    var url = req.protocol + "://" + req.headers.host;
    ClassObj
        .find()
        .select('_id studentId roomNo')
        .populate('teacherId', 'name sex subject')
        .populate('studentId', 'name sex')
        .exec()
        .then(function(docs) {
            var response = {
                count: docs.length,
                Classes: docs.map(function(doc) {
                    return {
                        roomNo: doc.roomNo,
                        teacher: doc.teacherId,
                        student: doc.studentId,
                        _id: doc._id,
                        request: {
                            type: "GET",
                            url: url + "/classes/" + doc._id
                        }
                    }
                })
            }
            res.status(200).json(response);
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).json({ error: err });
        });
}

exports.get_class = function(req, res, next) {
    var url = req.protocol + "://" + req.headers.host;
    var id = req.params.classId;
    ClassObj
        .findById(id)
        .select('_id studentId roomNo')
        .populate('teacherId', 'name sex subject')
        .populate('studentId', 'name sex')
        .exec()
        .then(function(doc) {
            console.log(doc);
            if(doc){
                res.status(200).json({
                    Class: {
                        roomNo: doc.roomNo,
                        teacher: doc.teacherId,
                        student: doc.studentId,
                        _id: doc._id,
                        request: {
                            type: "GET",
                            url: url + "/classes"
                        }
                    }
                });
            }else{
                res.status(404).json({ message: "Invalid class ID" })
            }
        })
        .catch(function(err) {
            console.log(err)
            res.status(500).json({ error: err });
        });
}

exports.create_class = function(req, res, next) {
    var url = req.protocol + "://" + req.headers.host;
    var classObj = new ClassObj({
        _id: new mongoose.Types.ObjectId(),
        teacherId: req.body.teacherId,
        studentId: req.body.studentId,
        roomNo: req.body.roomNo
    });
    classObj
        .save()
        .then(function(result) {
            console.log(result);
            res.status(201).json({
                message: "Class created",
                createStudent: {
                    roomNo: result.roomNo,
                    teacherId: result.teacherId,
                    studentId: result.studentId,
                    _id: result._id,
                    request: {
                        type: "GET",
                        url: url + "/classes/" + result._id
                    }
                }
            });
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).json(err);
        });
}

exports.update_class = function(req, res, next) {
    var url = req.protocol + "://" + req.headers.host + req.originalUrl;
    var id = req.params.classId;
    var updateOps = {};
    for(var ops of req.body) {
        updateOps[ops.opsName] = ops.value;
    }
    ClassObj.update({ _id: id }, { $set: updateOps })
        .exec()
        .then(function(result) {
            res.status(200).json({
                message: "Class updated",
                request: {
                    type: "GET",
                    url: url
                }
            });
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).json({ error: err });
        });
}

exports.delete_class = function(req, res, next) {
    var url = req.protocol + "://" + req.headers.host;
    var id = req.params.classId;
    ClassObj.remove({ _id: id })
        .exec()
        .then(function(result) {
            res.status(200).json({
                message: "Class deleted",
                request: {
                    type: "POST",
                    url: url + "/classes",
                    body: { name: "String", sex: "String" }
                }
            })
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).json({ error: err });
        });
}