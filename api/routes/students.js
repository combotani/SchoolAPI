var express = require('express');
var router = express.Router();

var studentController = require('../controllers/students');

router.get('/', studentController.get_all);

router.get('/:studentId', studentController.get_student);

router.post('/', studentController.create_student);

router.put('/:studentId', studentController.update_student);

router.delete('/:studentId', studentController.delete_student);

module.exports = router;