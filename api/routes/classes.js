var express = require('express');
var router = express.Router();

var classController = require('../controllers/classes');

router.get('/', classController.get_all);

router.get('/:classId', classController.get_class);

router.post('/', classController.create_class);

router.put('/:classId', classController.update_class);

router.delete('/:classId', classController.delete_class);

module.exports = router;