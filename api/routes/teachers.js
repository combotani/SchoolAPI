var express = require('express');
var router = express.Router();

var teacherController = require('../controllers/teachers');

router.get('/', teacherController.get_all);

router.get('/:teacherId', teacherController.get_teacher);

router.post('/', teacherController.create_teacher);

router.put('/:teacherId', teacherController.update_teacher);

router.delete('/:teacherId', teacherController.delete_teacher);

module.exports = router;