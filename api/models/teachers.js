var mongoose = require('mongoose');

var teacherSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: { type: String, required: true },
    sex: { type: String, required: true },
    subject: { type: String, required: true }
})

module.exports = mongoose.model('Teacher', teacherSchema);