var mongoose = require('mongoose');

var studentSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: { type: String, required: true },
    sex: { type: String, required: true }
})

module.exports = mongoose.model('Student', studentSchema);