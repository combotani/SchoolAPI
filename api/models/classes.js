var mongoose = require('mongoose');

var classesSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    studentId: { type: mongoose.Schema.Types.ObjectId, ref: 'Student', required: true },
    teacherId: { type: mongoose.Schema.Types.ObjectId, ref: 'Teacher' },
    roomNo: { type: Number, required: true }
});

module.exports = mongoose.model('Class', classesSchema);