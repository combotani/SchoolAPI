var express = require('express');
var app = express();
var mongoose = require('mongoose');

var logger = require('morgan');

var studentsRouter = require('./api/routes/students');
var teachersRouter = require('./api/routes/teachers');
var classesRouter = require('./api/routes/classes');

mongoose.connect('mongodb://admin:SchoolAPI@localhost:27017/SchoolAPI')
mongoose.Promise = global.Promise;

app.use(logger('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use('/students', studentsRouter);
app.use('/teachers', teachersRouter);
app.use('/classes', classesRouter);

app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({
        message: err.message
    })
});

module.exports = app;